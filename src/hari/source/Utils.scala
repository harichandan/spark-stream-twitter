package hari.source

import org.apache.log4j.Level
import org.apache.log4j.Logger

object Utils {
  def setupLogging() = {
    val logger = Logger.getRootLogger()
    logger.setLevel(Level.ERROR)
  }
  
  def setupTwitter() = {
    import scala.io.Source
    
    for (line <- Source.fromFile("data/twitter.txt").getLines()) {
      val fields = line.split(" ")
      System.setProperty("twitter4j.oauth." + fields(0), fields(1))
    }
  }
}