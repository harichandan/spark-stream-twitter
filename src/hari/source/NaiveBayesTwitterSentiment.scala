package hari.source

import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.sql.types._
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.ml._
import org.apache.spark.ml.feature._
import org.apache.spark.ml.classification.NaiveBayes

object NaiveBayesTwitterSentiment {
  /* 
   * The CSV file has the following data:
   * 0 - the polarity of the tweet (0 = negative, 2 = neutral, 4 = positive)
   * 1 - the id of the tweet (2087)
   * 2 - the date of the tweet (Sat May 16 23:58:44 UTC 2009)
   * 3 - the query (lyx). If there is no query, then this value is NO_QUERY.
   * 4 - the user that tweeted (robotickilldozr)
   * 5 - the text of the tweet (Lyx is cool)
   */

  Logger.getLogger("org").setLevel(Level.ERROR)
  val spark = SparkSession
    .builder()
    .master("local[*]")
    .appName("TwitterSentiment")
    .getOrCreate()

  // For implicit conversions like converting RDDs to DFs
  import spark.implicits._

  def schema(): StructType = {
    StructType(Array(
      StructField("Sentiment", DoubleType), // Sentiment value
      StructField("TweetID", IntegerType),
      StructField("Timestamp", StringType),
      StructField("Query", StringType),
      StructField("UserHandle", StringType),
      StructField("Tweet", StringType)))
  }

  def createDataFrame(path: String): DataFrame = {
    spark.sqlContext.read.format("com.databricks.spark.csv")
      .option("header", "true")
      .schema(schema)
      .load(path)
      .drop("TweetID")
      .drop("Timestamp")
      .drop("Query")
      .drop("UserHandle")
  }

  /*
   * Function to remove hashtags and user handles
   */
  def cleanTweet(df: DataFrame): DataFrame = {
    df.withColumn("Tweet", regexp_replace(col("Tweet"), "[@#]\\w+", ""))
  }

  def tokenize(df: DataFrame): DataFrame = {
    new RegexTokenizer()
      .setInputCol("Tweet")
      .setOutputCol("tokens")
      .setPattern("\\W+")
      .transform(df)
  }

  def removeStopWords(df: DataFrame): DataFrame = {
    new StopWordsRemover()
      .setInputCol("tokens")
      .setOutputCol("tokensWithoutStopWords")
      .transform(df)
  }

  def countVectorize(df: DataFrame): DataFrame = {
    new CountVectorizer()
      .setInputCol("tokensWithoutStopWords")
      .setOutputCol("features")
      .setVocabSize(1000)
      .fit(df)
      .transform(df)
  }

  def binarize(df: DataFrame): DataFrame = {
    new Binarizer()
      .setInputCol("Sentiment")
      .setOutputCol("label")
      .setThreshold(1)
      .transform(df)
  }

  def main(args: Array[String]): Unit = {
    /*
     * Create the training DF using the Tweet and Sentiment columns
     */
    val trainingDF = cleanTweet(createDataFrame("data/twitter_train.csv"))
    val testDF = cleanTweet(createDataFrame("data/twitter_test.csv"))

    val tokenizedDF = tokenize(trainingDF)
    val tokenizedTestDF = tokenize(testDF)

    val removeStopWordsDF = removeStopWords(tokenizedDF)
    val removeStopWordsTestDF = removeStopWords(tokenizedTestDF)

    val countVectorizedDF = countVectorize(removeStopWordsDF)
    val countVectorizedTestDF = countVectorize(removeStopWordsTestDF)

    val binarizedDF = binarize(countVectorizedDF)
    val binarizedTestDF = binarize(countVectorizedTestDF)

    binarizedDF.show()

    val model = new NaiveBayes().fit(binarizedDF)
    val predictions = model.transform(binarizedTestDF)

    predictions.show()
  }
}