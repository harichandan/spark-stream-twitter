package hari.source

import org.apache.spark._
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.Seconds
import org.apache.spark.streaming.twitter._
import org.apache.log4j.Level
import org.apache.log4j.Logger

import Utils._

object StreamTweet {
  def cleanTweet(tweet: String): String = {
    tweet.split(" ")
         .filterNot(_.startsWith("#"))
         .filterNot(_.startsWith("@"))
         .filterNot(_.contains("http"))
         .filterNot(_.equals("rt"))
         .mkString(" ")
  }
  
  def main(args: Array[String]) {
    /*
     *  Set logging to ERROR level
     */
    Logger.getLogger("org").setLevel(Level.ERROR)

    /*
     *  Read the credentials file and authenticate using OAuth
     */
    setupTwitter()

    /*
     * Create a streaming context
     */
    val stc = new StreamingContext("local[*]", "StreamTweets", Seconds(5))

    /*
     *  Create the twitter stream
     */
    val tweets = TwitterUtils.createStream(stc, None)

    /*
     *  Filter out non-English tweets and get their text
     */
    val statuses = tweets.filter(_.getLang() == "en")
      .map(x => cleanTweet(x.getText().toLowerCase()))

    statuses.print()
    stc.start()
    stc.awaitTermination()
  }
}
